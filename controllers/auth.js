const {response} = require('express');
const Usuario = require('../models/Usuario');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');


const crearUsuario = async (req,res = response) =>{
    const {email, name, password} = req.body

    try {
    //Verificar el email
    const usuario = await Usuario.findOne({email: email});
    if(usuario){
        return res.status(400).json({
            ok: false,
            msg: 'El usuario ya existe con ese email'
        })
    }
    //Crear usuario con el modelo
    const dbUser = new Usuario(req.body)
    //cifrar contraseña
    const salt = bcrypt.genSaltSync();
    dbUser.password = bcrypt.hashSync(password, salt);
    //generar JWT 
    const token = await generarJWT(dbUser.id, name)
    //Crear usuario de DB
    await dbUser.save();
    //generar response exitoso
    return res.status(201).json({
        ok: true,
        uid: dbUser.id,
        name: name,
        email: email,
        token: token
    });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            msg: 'Algo salio mal'
        });
    }
}

const loginUsuario = async (req, res) =>{
    const {email, password} = req.body;
    try {
        const dbUser = await Usuario.findOne({email: email});
        if(!dbUser){
            return res.status(400).json({
                ok: false,
                msg: 'Correo o contraseña incorrecto'
            });
        }
        //validar si la contraseña es correcta
        const validPassword = bcrypt.compareSync(password, dbUser.password);
        if(!validPassword){
            return res.status(400).json({
                ok: false,
                msg: 'Correo o contraseña incorrecto'
            });
        }
        //generar JWT
        const token = await generarJWT(dbUser.id, dbUser.name);
        //response
        return res.json({
            ok: true,
            uid: dbUser.id,
            name: dbUser.name,
            email: dbUser.email,
            token: token
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }

}

const revalidarToken = async (req ,res) =>{
    const { uid } = req;
    //leer la bd
    const dbUser = await Usuario.findById(uid);
     //generar JWT
     const token = await generarJWT(uid, dbUser.name);
    return res.json({
        ok: true,
        uid: uid,
        name: dbUser.name,
        email: dbUser.email,
        token: token
    })
}

module.exports = {
    crearUsuario,
    loginUsuario,
    revalidarToken
}